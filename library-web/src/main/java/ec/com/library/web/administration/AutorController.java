package ec.com.library.web.administration;

import ec.com.library.application.factory.LibraryFactory;
import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Autor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Collection;

/**
 * Created by Roberto on 4/5/2018.
 */

@RestController
public class AutorController {
    @RequestMapping(value = "/buscarAutorPorId/{autorId}", method = RequestMethod.GET, headers = "Accept=*/*", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Autor buscarAutorPorId(@PathVariable Integer autorId, HttpSession session) throws LibraryGenericException {
        return LibraryFactory.getInstance().getAutorService().buscar(autorId);
    }

    @RequestMapping(value = "/buscarAutores", method = RequestMethod.GET, headers = "Accept=*/*", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Collection<Autor> buscarAutores(HttpSession session) throws LibraryGenericException {
        return LibraryFactory.getInstance().getAutorService().buscarTodos();
    }


    @RequestMapping(value = "/createAutor_json", method = RequestMethod.POST)
    public @ResponseBody
    Autor createPerson_json(@RequestBody Autor autor, HttpSession session) throws LibraryGenericException {
        if (autor.getIdAutor() == null) {
            LibraryFactory.getInstance().getAutorService().insertar(autor);
        } else {
            LibraryFactory.getInstance().getAutorService().actualizar(autor);
        }

        return autor;
    }
}
