package ec.com.library.web.administration;

import ec.com.library.application.factory.LibraryFactory;
import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Autor;
import ec.com.library.core.entities.Categoria;
import ec.com.library.core.entities.Editorial;
import ec.com.library.core.entities.EditorialCategoria;
import ec.com.library.core.entities.Estanteria;
import ec.com.library.core.entities.Libro;
import ec.com.library.core.entities.LibroAutor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.Collection;

/**
 * Created by Roberto on 4/6/2018.
 */
@RestController
@RequestMapping("/adm")
public class AdministracionController {

    @RequestMapping(value = "/buscarAutores", method = RequestMethod.GET, headers = "Accept=*/*", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Collection<Autor> buscarAutores(HttpSession session) throws LibraryGenericException {
        return LibraryFactory.getInstance().getAutorService().buscarTodos();
    }


    @RequestMapping(value = "/createAutor_json", method = RequestMethod.POST)
    public @ResponseBody
    Autor createAutor_json(@RequestBody Autor autor, HttpSession session) throws LibraryGenericException {
        if (autor.getIdAutor() == null) {
            LibraryFactory.getInstance().getAutorService().insertar(autor);
        } else {
            LibraryFactory.getInstance().getAutorService().actualizar(autor);
        }

        return autor;
    }

    @RequestMapping(value = "/buscarCategorias", method = RequestMethod.GET, headers = "Accept=*/*", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Collection<Categoria> buscarCategorias(HttpSession session) throws LibraryGenericException {
        return LibraryFactory.getInstance().getAdministracionService().buscarCategoriaTodos();
    }


    @RequestMapping(value = "/createCategoria_json", method = RequestMethod.POST)
    public @ResponseBody
    Categoria createCategoriajson(@RequestBody Categoria entity, HttpSession session) throws LibraryGenericException {
        if (entity.getIdCategoria() == null) {
            LibraryFactory.getInstance().getAdministracionService().insertar(entity);
        } else {
            LibraryFactory.getInstance().getAdministracionService().actualizar(entity);
        }

        return entity;
    }


    @RequestMapping(value = "/buscarEditorialCategoria", method = RequestMethod.GET, headers = "Accept=*/*", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Collection<EditorialCategoria> buscarEditorialCategoria(HttpSession session) throws LibraryGenericException {
        return LibraryFactory.getInstance().getAdministracionService().buscarEditorialCategoriaTodos();
    }


    @RequestMapping(value = "/createEditorialCategoria_json", method = RequestMethod.POST)
    public @ResponseBody
    EditorialCategoria createEditorialCategoriajson(@RequestBody EditorialCategoria entity, HttpSession session) throws LibraryGenericException {
        if (entity.getIdEditorialCategoria() == null) {
            LibraryFactory.getInstance().getAdministracionService().insertar(entity);
        } else {
            LibraryFactory.getInstance().getAdministracionService().actualizar(entity);
        }

        return entity;
    }


    @RequestMapping(value = "/buscarEditorial", method = RequestMethod.GET, headers = "Accept=*/*", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Collection<Editorial> buscarEditorial(HttpSession session) throws LibraryGenericException {
        return LibraryFactory.getInstance().getAdministracionService().buscarEditorialTodos();
    }


    @RequestMapping(value = "/createEditorial_json", method = RequestMethod.POST)
    public @ResponseBody
    Editorial createEditorialjson(@RequestBody Editorial entity, HttpSession session) throws LibraryGenericException {
        if (entity.getIdEditorial() == null) {
            LibraryFactory.getInstance().getAdministracionService().insertar(entity);
        } else {
            LibraryFactory.getInstance().getAdministracionService().actualizar(entity);
        }

        return entity;
    }


    @RequestMapping(value = "/buscarEstanteria", method = RequestMethod.GET, headers = "Accept=*/*", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Collection<Estanteria> buscarEstanteria(HttpSession session) throws LibraryGenericException {
        return LibraryFactory.getInstance().getAdministracionService().buscarEstanteriaTodos();
    }


    @RequestMapping(value = "/createEstanteria_json", method = RequestMethod.POST)
    public @ResponseBody
    Estanteria createEstanteriajson(@RequestBody Estanteria entity, HttpSession session) throws LibraryGenericException {
        if (entity.getIdEstanteria() == null) {
            LibraryFactory.getInstance().getAdministracionService().insertar(entity);
        } else {
            LibraryFactory.getInstance().getAdministracionService().actualizar(entity);
        }

        return entity;
    }


    @RequestMapping(value = "/buscarLibroAutor", method = RequestMethod.GET, headers = "Accept=*/*", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Collection<LibroAutor> buscarLibroAutor(HttpSession session) throws LibraryGenericException {
        return LibraryFactory.getInstance().getAdministracionService().buscarLibroAutorTodos();
    }


    @RequestMapping(value = "/createLibroAutor_json", method = RequestMethod.POST)
    public @ResponseBody
    LibroAutor createEstanteriajson(@RequestBody LibroAutor entity, HttpSession session) throws LibraryGenericException {
        if (entity.getIdLibroAutor() == null) {
            LibraryFactory.getInstance().getAdministracionService().insertar(entity);
        } else {
            LibraryFactory.getInstance().getAdministracionService().actualizar(entity);
        }

        return entity;
    }


    @RequestMapping(value = "/buscarLibro", method = RequestMethod.GET, headers = "Accept=*/*", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Collection<Libro> buscarLibro(HttpSession session) throws LibraryGenericException {
        return LibraryFactory.getInstance().getAdministracionService().buscarLibroTodos();
    }


    @RequestMapping(value = "/createLibro_json", method = RequestMethod.POST)
    public @ResponseBody
    Libro createEstanteriajson(@RequestBody Libro entity, HttpSession session) throws LibraryGenericException {
        if (entity.getIdLibro() == null) {
            entity.setFechaCreacion(Calendar.getInstance().getTime());
            LibraryFactory.getInstance().getAdministracionService().insertar(entity);
        } else {
            LibraryFactory.getInstance().getAdministracionService().actualizar(entity);
        }

        return entity;
    }
}
