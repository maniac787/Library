app.controller('EstanteriaController', ['$q', '$scope', '$timeout', '$http', 'Alertify', 'blockUI', 'configuracionLibService',
    function ($q, $scope, $timeout, $http, Alertify, blockUI, configuracionLibService) {
        var ctrl = this;

        ctrl.lstEntities = null;
        ctrl.lstCategorias = null;
        ctrl.objEntity = null;
        ctrl.host = configuracionLibService.hostConfig();

        ctrl.buscarAction = function () {
            blockUI.start();
            ctrl.objEntity = null;

            $http.get(ctrl.host + '/buscarEstanteria').then(function (data) {
                ctrl.lstEntities = data.data;
                Alertify.success('Datos cargados!');
                blockUI.stop();
            }, function (error) {
                blockUI.stop();
            });
        };

        ctrl.nuevoAction = function () {
            ctrl.objEntity = {};

            $http.get(ctrl.host + '/buscarCategorias').then(function (data) {
                ctrl.lstCategorias = data.data;
            }, function (error) {
            });

        };

        ctrl.editarAction = function (autor) {
            ctrl.objEntity = autor;
            $http.get(ctrl.host + '/buscarCategorias').then(function (data) {
                ctrl.lstCategorias = data.data;
            }, function (error) {
            });
        };

        ctrl.guardarAction = function () {
            blockUI.start();
            var response = $http.post(ctrl.host + '/createEstanteria_json.json', ctrl.objEntity);
            response.success(function (data, status, headers, config) {
                Alertify.success('Registro actualizado!');
                blockUI.stop();
            });

            response.error(function (data, status, headers, config) {
                Alertify.error('Ocurrio un error!');
                blockUI.stop();
            });
        };
    }]
);