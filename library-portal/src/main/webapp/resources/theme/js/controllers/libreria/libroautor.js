app.controller('LibroautorController', ['$q', '$scope', '$timeout', '$http', 'Alertify', 'blockUI', 'configuracionLibService',
    function ($q, $scope, $timeout, $http, Alertify, blockUI, configuracionLibService) {
        var ctrl = this;

        ctrl.lstEntities = null;
        ctrl.objEntity = null;
        ctrl.lstautores = null;
        ctrl.lstLibros = null;

        ctrl.host = configuracionLibService.hostConfig();

        ctrl.buscarAction = function () {
            blockUI.start();
            ctrl.objEntity = null;

            $http.get(ctrl.host + '/buscarLibroAutor').then(function (data) {
                ctrl.lstEntities = data.data;
                Alertify.success('Datos cargados!');
                blockUI.stop();
            }, function (error) {
                blockUI.stop();
            });
        };

        ctrl.cargarListas = function () {
            $http.get(ctrl.host + '/buscarAutores').then(function (data) {
                ctrl.lstautores = data.data;
            }, function (error) {
            });

            $http.get(ctrl.host + '/buscarLibro').then(function (data) {
                ctrl.lstLibros = data.data;
            }, function (error) {
            });
        };

        ctrl.nuevoAction = function () {
            ctrl.objEntity = {};
            ctrl.cargarListas();

        };

        ctrl.editarAction = function (autor) {
            ctrl.objEntity = autor;
            ctrl.cargarListas();
        };

        ctrl.guardarAction = function () {
            blockUI.start();
            var response = $http.post(ctrl.host + '/createLibroAutor_json.json', ctrl.objEntity);
            response.success(function (data, status, headers, config) {
                Alertify.success('Registro actualizado!');
                blockUI.stop();
            });

            response.error(function (data, status, headers, config) {
                Alertify.error('Ocurrio un error!');
                blockUI.stop();
            });
        };
    }]
);