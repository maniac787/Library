'use strict';

app.controller('SigninFormController', ['$cookies', '$scope','$timeout', '$http', '$state',
    function ($cookies, $scope, $timeout, $http, $state) {

        $scope.redireccionar = function (){
            $state.go('app.administracion.autor');
        };

    }])
;