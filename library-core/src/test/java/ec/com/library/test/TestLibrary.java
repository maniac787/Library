package ec.com.library.test;

import ec.com.library.application.factory.LibraryFactory;
import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Autor;
import org.junit.Test;

import java.util.Collection;

/**
 * Created by Roberto on 4/5/2018.
 */
public class TestLibrary {
    @Test
    public void testfactory() {
        Collection<Autor> autors = null;
        try {
            autors = LibraryFactory.getInstance().getAutorService().buscarTodos();
            for (Autor autor : autors) {
                System.out.println(autor);
            }

        } catch (LibraryGenericException e) {
            e.printStackTrace();
        }
    }
}
