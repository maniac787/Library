package ec.com.library.application.services.administracion;

import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Categoria;
import ec.com.library.core.entities.Editorial;
import ec.com.library.core.entities.EditorialCategoria;
import ec.com.library.core.entities.Estanteria;
import ec.com.library.core.entities.Libro;
import ec.com.library.core.entities.LibroAutor;

import java.util.List;

/**
 * Created by Roberto on 4/5/2018.
 */
public interface IAdministracionService {
    List<Categoria> buscarCategoriaTodos() throws LibraryGenericException;

    void insertar(Categoria entity) throws LibraryGenericException;

    void actualizar(Categoria entity) throws LibraryGenericException;

    List<EditorialCategoria> buscarEditorialCategoriaTodos() throws LibraryGenericException;

    void insertar(EditorialCategoria entity) throws LibraryGenericException;

    void actualizar(EditorialCategoria entity) throws LibraryGenericException;


    List<Editorial> buscarEditorialTodos() throws LibraryGenericException;

    void insertar(Editorial entity) throws LibraryGenericException;

    void actualizar(Editorial entity) throws LibraryGenericException;


    List<Estanteria> buscarEstanteriaTodos() throws LibraryGenericException;

    void insertar(Estanteria entity) throws LibraryGenericException;

    void actualizar(Estanteria entity) throws LibraryGenericException;

    List<LibroAutor> buscarLibroAutorTodos() throws LibraryGenericException;

    void insertar(LibroAutor entity) throws LibraryGenericException;

    void actualizar(LibroAutor entity) throws LibraryGenericException;


    List<Libro> buscarLibroTodos() throws LibraryGenericException;

    void insertar(Libro entity) throws LibraryGenericException;

    void actualizar(Libro entity) throws LibraryGenericException;
}
