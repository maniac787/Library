package ec.com.library.application.services.administracion;

import ec.com.library.application.services.IServices;
import ec.com.library.core.entities.LibroAutor;

/**
 * Created by Roberto on 4/5/2018.
 */
public interface ILibroAutorService extends IServices<LibroAutor> {
}
