package ec.com.library.application.services.administracion.impl;

import ec.com.library.application.services.administracion.ILibroService;
import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Libro;
import ec.com.library.core.repositories.administracion.ILibroRepository;

import java.util.List;

/**
 * Created by Roberto on 4/5/2018.
 */
public class LibroServiceImpl implements ILibroService {

    private ILibroRepository libroRepository;

    @Override
    public Libro buscar(Integer id) throws LibraryGenericException {
        return libroRepository.buscar(id);

    }

    @Override
    public List<Libro> buscarTodos() throws LibraryGenericException {
        return libroRepository.buscarTodos();
    }

    @Override
    public void insertar(Libro entity) throws LibraryGenericException {
        libroRepository.insertar(entity);
    }

    @Override
    public Libro actualizar(Libro entity) throws LibraryGenericException {
        return libroRepository.actualizar(entity);
    }

    @Override
    public void borrar(Libro entity) throws LibraryGenericException {
        libroRepository.borrar(entity);
    }

    public void setLibroRepository(ILibroRepository libroRepository) {
        this.libroRepository = libroRepository;
    }
}
