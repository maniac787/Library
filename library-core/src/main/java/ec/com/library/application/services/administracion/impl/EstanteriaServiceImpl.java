package ec.com.library.application.services.administracion.impl;

import ec.com.library.application.services.administracion.IEstanteriaService;
import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Estanteria;
import ec.com.library.core.repositories.administracion.IEstanteriaRepository;

import java.util.List;

/**
 * Created by Roberto on 4/5/2018.
 */
public class EstanteriaServiceImpl implements IEstanteriaService {

    private IEstanteriaRepository estanteriaRepository;

    @Override
    public Estanteria buscar(Integer id) throws LibraryGenericException {
        return estanteriaRepository.buscar(id);

    }

    @Override
    public List<Estanteria> buscarTodos() throws LibraryGenericException {
        return estanteriaRepository.buscarTodos();
    }

    @Override
    public void insertar(Estanteria entity) throws LibraryGenericException {
        estanteriaRepository.insertar(entity);
    }

    @Override
    public Estanteria actualizar(Estanteria entity) throws LibraryGenericException {
        return estanteriaRepository.actualizar(entity);
    }

    @Override
    public void borrar(Estanteria entity) throws LibraryGenericException {
        estanteriaRepository.borrar(entity);
    }

    public void setEstanteriaRepository(IEstanteriaRepository estanteriaRepository) {
        this.estanteriaRepository = estanteriaRepository;
    }
}
