package ec.com.library.application.services.administracion.impl;

import ec.com.library.application.services.administracion.IAutorService;
import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Autor;
import ec.com.library.core.repositories.administracion.IAutorRepository;

import java.util.List;

/**
 * Created by Roberto on 4/5/2018.
 */
public class AutorServiceImpl implements IAutorService {

    private IAutorRepository autorRepository;

    @Override
    public Autor buscar(Integer id) throws LibraryGenericException {
        return autorRepository.buscar(id);

    }

    @Override
    public List<Autor> buscarTodos() throws LibraryGenericException {
        return autorRepository.buscarTodos();
    }

    @Override
    public void insertar(Autor entity) throws LibraryGenericException {
        autorRepository.insertar(entity);
    }

    @Override
    public Autor actualizar(Autor entity) throws LibraryGenericException {
        return autorRepository.actualizar(entity);
    }

    @Override
    public void borrar(Autor entity) throws LibraryGenericException {
        autorRepository.borrar(entity);
    }

    public void setAutorRepository(IAutorRepository autorRepository) {
        this.autorRepository = autorRepository;
    }
}
