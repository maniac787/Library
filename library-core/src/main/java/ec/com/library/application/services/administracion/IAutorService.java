package ec.com.library.application.services.administracion;

import ec.com.library.application.services.IServices;
import ec.com.library.core.entities.Autor;

/**
 * Created by Roberto on 4/5/2018.
 */
public interface IAutorService extends IServices<Autor> {
}
