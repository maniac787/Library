package ec.com.library.application.services.administracion.impl;

import ec.com.library.application.services.administracion.ICategoriaService;
import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Categoria;
import ec.com.library.core.repositories.administracion.ICategoriaRepository;

import java.util.List;

/**
 * Created by Roberto on 4/5/2018.
 */
public class CategoriaServiceImpl implements ICategoriaService {

    private ICategoriaRepository categoriaRepository;

    @Override
    public Categoria buscar(Integer id) throws LibraryGenericException {
        return categoriaRepository.buscar(id);

    }

    @Override
    public List<Categoria> buscarTodos() throws LibraryGenericException {
        return categoriaRepository.buscarTodos();
    }

    @Override
    public void insertar(Categoria entity) throws LibraryGenericException {
        categoriaRepository.insertar(entity);
    }

    @Override
    public Categoria actualizar(Categoria entity) throws LibraryGenericException {
        return categoriaRepository.actualizar(entity);
    }

    @Override
    public void borrar(Categoria entity) throws LibraryGenericException {
        categoriaRepository.borrar(entity);
    }

    public void setCategoriaRepository(ICategoriaRepository categoriaRepository) {
        this.categoriaRepository = categoriaRepository;
    }
}
