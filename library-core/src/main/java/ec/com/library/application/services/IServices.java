package ec.com.library.application.services;

import ec.com.library.core.Exception.LibraryGenericException;

import java.io.Serializable;
import java.util.List;

/**
 * INterfaz que permite invocxar el crud de la capa de repositorio
 * Created by Roberto on 4/5/2018.
 */
public interface IServices<T extends Serializable> {

    /**
     * Busqueda por Id
     *
     * @param id
     * @return
     * @throws LibraryGenericException
     */
    T buscar(final Integer id) throws LibraryGenericException;

    /**
     * Buscar todos los registos
     *
     * @return
     * @throws LibraryGenericException
     */
    List<T> buscarTodos() throws LibraryGenericException;

    /**
     * Persiste un nuevo reguistro
     *
     * @param entity
     * @throws LibraryGenericException
     */
    void insertar(final T entity) throws LibraryGenericException;

    /**
     * Actualiza un registro existente
     *
     * @param entity
     * @return
     * @throws LibraryGenericException
     */
    T actualizar(final T entity) throws LibraryGenericException;

    /**
     * Borrado logico, actualiza las propiedades eliminado y activo
     *
     * @param entity
     * @throws LibraryGenericException
     */
    void borrar(final T entity) throws LibraryGenericException;
}
