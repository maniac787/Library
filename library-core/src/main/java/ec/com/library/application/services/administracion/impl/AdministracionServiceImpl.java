package ec.com.library.application.services.administracion.impl;

import ec.com.library.application.services.administracion.IAdministracionService;
import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Categoria;
import ec.com.library.core.entities.Editorial;
import ec.com.library.core.entities.EditorialCategoria;
import ec.com.library.core.entities.Estanteria;
import ec.com.library.core.entities.Libro;
import ec.com.library.core.entities.LibroAutor;
import ec.com.library.core.repositories.administracion.IAutorRepository;
import ec.com.library.core.repositories.administracion.ICategoriaRepository;
import ec.com.library.core.repositories.administracion.IEditorialCategoriaRepository;
import ec.com.library.core.repositories.administracion.IEditorialRepository;
import ec.com.library.core.repositories.administracion.IEstanteriaRepository;
import ec.com.library.core.repositories.administracion.ILibroAutorRepository;
import ec.com.library.core.repositories.administracion.ILibroRepository;

import java.util.List;

/**
 * Created by Roberto on 4/5/2018.
 */
public class AdministracionServiceImpl implements IAdministracionService {
    private IAutorRepository autorRepository;
    private ICategoriaRepository categoriaRepository;
    private IEditorialCategoriaRepository editorialCategoriaRepository;
    private IEditorialRepository editorialRepository;
    private IEstanteriaRepository estanteriaRepository;
    private ILibroAutorRepository libroAutorRepository;
    private ILibroRepository libroRepository;


    public void setAutorRepository(IAutorRepository autorRepository) {
        this.autorRepository = autorRepository;
    }

    public void setCategoriaRepository(ICategoriaRepository categoriaRepository) {
        this.categoriaRepository = categoriaRepository;
    }

    public void setEditorialCategoriaRepository(IEditorialCategoriaRepository editorialCategoriaRepository) {
        this.editorialCategoriaRepository = editorialCategoriaRepository;
    }

    public void setEditorialRepository(IEditorialRepository editorialRepository) {
        this.editorialRepository = editorialRepository;
    }

    public void setEstanteriaRepository(IEstanteriaRepository estanteriaRepository) {
        this.estanteriaRepository = estanteriaRepository;
    }

    public void setLibroAutorRepository(ILibroAutorRepository libroAutorRepository) {
        this.libroAutorRepository = libroAutorRepository;
    }

    public IAutorRepository getAutorRepository() {
        return autorRepository;
    }

    public ICategoriaRepository getCategoriaRepository() {
        return categoriaRepository;
    }

    public IEditorialCategoriaRepository getEditorialCategoriaRepository() {
        return editorialCategoriaRepository;
    }

    public IEditorialRepository getEditorialRepository() {
        return editorialRepository;
    }

    public IEstanteriaRepository getEstanteriaRepository() {
        return estanteriaRepository;
    }

    public ILibroAutorRepository getLibroAutorRepository() {
        return libroAutorRepository;
    }

    public ILibroRepository getLibroRepository() {
        return libroRepository;
    }

    public void setLibroRepository(ILibroRepository libroRepository) {
        this.libroRepository = libroRepository;
    }

    @Override
    public List<Categoria> buscarCategoriaTodos() throws LibraryGenericException {
        return categoriaRepository.buscarTodos();
    }

    @Override
    public void insertar(Categoria entity) throws LibraryGenericException {
        categoriaRepository.insertar(entity);
    }

    @Override
    public void actualizar(Categoria entity) throws LibraryGenericException {
        categoriaRepository.actualizar(entity);
    }

    @Override
    public List<EditorialCategoria> buscarEditorialCategoriaTodos() throws LibraryGenericException {
        return editorialCategoriaRepository.buscarTodos();
    }

    @Override
    public void insertar(EditorialCategoria entity) throws LibraryGenericException {
        editorialCategoriaRepository.insertar(entity);
    }

    @Override
    public void actualizar(EditorialCategoria entity) throws LibraryGenericException {
        editorialCategoriaRepository.actualizar(entity);
    }

    @Override
    public List<Editorial> buscarEditorialTodos() throws LibraryGenericException {
        return editorialRepository.buscarTodos();
    }

    @Override
    public void insertar(Editorial entity) throws LibraryGenericException {
        editorialRepository.insertar(entity);
    }

    @Override
    public void actualizar(Editorial entity) throws LibraryGenericException {
        editorialRepository.actualizar(entity);
    }

    @Override
    public List<Estanteria> buscarEstanteriaTodos() throws LibraryGenericException {
        return estanteriaRepository.buscarTodos();
    }

    @Override
    public void insertar(Estanteria entity) throws LibraryGenericException {
        estanteriaRepository.insertar(entity);
    }

    @Override
    public void actualizar(Estanteria entity) throws LibraryGenericException {
        estanteriaRepository.actualizar(entity);
    }

    @Override
    public List<LibroAutor> buscarLibroAutorTodos() throws LibraryGenericException {
        return libroAutorRepository.buscarTodos();
    }

    @Override
    public void insertar(LibroAutor entity) throws LibraryGenericException {
        libroAutorRepository.insertar(entity);
    }

    @Override
    public void actualizar(LibroAutor entity) throws LibraryGenericException {
        libroAutorRepository.actualizar(entity);
    }

    @Override
    public List<Libro> buscarLibroTodos() throws LibraryGenericException {
        return libroRepository.buscarTodos();
    }

    @Override
    public void insertar(Libro entity) throws LibraryGenericException {
        libroRepository.insertar(entity);
    }

    @Override
    public void actualizar(Libro entity) throws LibraryGenericException {
        libroRepository.actualizar(entity);
    }
}
