package ec.com.library.application.services.administracion.impl;

import ec.com.library.application.services.administracion.ILibroAutorService;
import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.LibroAutor;
import ec.com.library.core.repositories.administracion.ILibroAutorRepository;

import java.util.List;

/**
 * Created by Roberto on 4/5/2018.
 */
public class LibroAutorServiceImpl implements ILibroAutorService {

    private ILibroAutorRepository libroAutorRepository;

    @Override
    public LibroAutor buscar(Integer id) throws LibraryGenericException {
        return libroAutorRepository.buscar(id);

    }

    @Override
    public List<LibroAutor> buscarTodos() throws LibraryGenericException {
        return libroAutorRepository.buscarTodos();
    }

    @Override
    public void insertar(LibroAutor entity) throws LibraryGenericException {
        libroAutorRepository.insertar(entity);
    }

    @Override
    public LibroAutor actualizar(LibroAutor entity) throws LibraryGenericException {
        return libroAutorRepository.actualizar(entity);
    }

    @Override
    public void borrar(LibroAutor entity) throws LibraryGenericException {
        libroAutorRepository.borrar(entity);
    }

    public void setLibroAutorRepository(ILibroAutorRepository libroAutorRepository) {
        this.libroAutorRepository = libroAutorRepository;
    }
}
