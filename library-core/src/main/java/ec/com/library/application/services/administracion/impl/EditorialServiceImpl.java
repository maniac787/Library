package ec.com.library.application.services.administracion.impl;

import ec.com.library.application.services.administracion.IEditorialService;
import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Editorial;
import ec.com.library.core.repositories.administracion.IEditorialRepository;

import java.util.List;

/**
 * Created by Roberto on 4/5/2018.
 */
public class EditorialServiceImpl implements IEditorialService {

    private IEditorialRepository editorialRepository;

    @Override
    public Editorial buscar(Integer id) throws LibraryGenericException {
        return editorialRepository.buscar(id);

    }

    @Override
    public List<Editorial> buscarTodos() throws LibraryGenericException {
        return editorialRepository.buscarTodos();
    }

    @Override
    public void insertar(Editorial entity) throws LibraryGenericException {
        editorialRepository.insertar(entity);
    }

    @Override
    public Editorial actualizar(Editorial entity) throws LibraryGenericException {
        return editorialRepository.actualizar(entity);
    }

    @Override
    public void borrar(Editorial entity) throws LibraryGenericException {
        editorialRepository.borrar(entity);
    }

    public void setEditorialRepository(IEditorialRepository editorialRepository) {
        this.editorialRepository = editorialRepository;
    }
}
