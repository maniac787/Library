package ec.com.library.application.services.administracion;

import ec.com.library.application.services.IServices;
import ec.com.library.core.entities.EditorialCategoria;

/**
 * Created by Roberto on 4/5/2018.
 */
public interface IEditorialCategoriaService extends IServices<EditorialCategoria> {
}
