package ec.com.library.application.services.administracion;

import ec.com.library.application.services.IServices;
import ec.com.library.core.entities.Categoria;

/**
 * Created by Roberto on 4/5/2018.
 */
public interface ICategoriaService extends IServices<Categoria> {
}
