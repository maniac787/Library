package ec.com.library.application.services.administracion.impl;

import ec.com.library.application.services.administracion.IEditorialCategoriaService;
import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.EditorialCategoria;
import ec.com.library.core.repositories.administracion.IEditorialCategoriaRepository;

import java.util.List;

/**
 * Created by Roberto on 4/5/2018.
 */
public class EditorialCategoriaServiceImpl implements IEditorialCategoriaService {

    private IEditorialCategoriaRepository editorialCategoriaRepository;

    @Override
    public EditorialCategoria buscar(Integer id) throws LibraryGenericException {
        return editorialCategoriaRepository.buscar(id);

    }

    @Override
    public List<EditorialCategoria> buscarTodos() throws LibraryGenericException {
        return editorialCategoriaRepository.buscarTodos();
    }

    @Override
    public void insertar(EditorialCategoria entity) throws LibraryGenericException {
        editorialCategoriaRepository.insertar(entity);
    }

    @Override
    public EditorialCategoria actualizar(EditorialCategoria entity) throws LibraryGenericException {
        return editorialCategoriaRepository.actualizar(entity);
    }

    @Override
    public void borrar(EditorialCategoria entity) throws LibraryGenericException {
        editorialCategoriaRepository.borrar(entity);
    }

    public void setEditorialCategoriaRepository(IEditorialCategoriaRepository editorialCategoriaRepository) {
        this.editorialCategoriaRepository = editorialCategoriaRepository;
    }
}
