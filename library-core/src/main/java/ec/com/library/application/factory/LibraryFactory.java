package ec.com.library.application.factory;

import ec.com.library.application.services.administracion.IAdministracionService;
import ec.com.library.application.services.administracion.IAutorService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Permite instanciar los servicios mediante spring
 * Created by Roberto on 4/5/2018.
 */
public class LibraryFactory {

    private static final LibraryFactory LIBRARY_FACTORY = new LibraryFactory();

    private ApplicationContext context = null;

    private LibraryFactory() {
        context = new ClassPathXmlApplicationContext(new String[]{
                "ec/com/library/core/spring/config/core-application-context.xml",
                "ec/com/library/core/spring/config/repositories.xml",
                "ec/com/library/core/spring/config/services.xml",
                "ec/com/library/core/spring/config/transaction.xml"});
    }

    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    public static LibraryFactory getInstance() {
        return LIBRARY_FACTORY;
    }

    public IAutorService getAutorService() {
        return context.getBean("autor-service", IAutorService.class);
    }

    public IAdministracionService getAdministracionService() {
        return context.getBean("administration-service", IAdministracionService.class);
    }
}
