/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.library.core.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Roberto
 */
@Entity
@Table(name = "editorial_categoria")
@NamedQueries({
    @NamedQuery(name = "EditorialCategoria.findAll", query = "SELECT e FROM EditorialCategoria e")})
public class EditorialCategoria extends Audit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_editorial_categoria")
    private Integer idEditorialCategoria;
    @OneToMany(mappedBy = "editorialCategoria", fetch = FetchType.LAZY)
    private Collection<Libro> libroCollection;
    @JoinColumn(name = "id_categoria", referencedColumnName = "id_categoria")
    @ManyToOne(fetch = FetchType.EAGER)
    private Categoria categoria;
    @JoinColumn(name = "id_editorial", referencedColumnName = "id_editorial")
    @ManyToOne(fetch = FetchType.EAGER)
    private Editorial editorial;

    public EditorialCategoria() {
    }

    public EditorialCategoria(Integer idEditorialCategoria) {
        this.idEditorialCategoria = idEditorialCategoria;
    }

    public Integer getIdEditorialCategoria() {
        return idEditorialCategoria;
    }

    public void setIdEditorialCategoria(Integer idEditorialCategoria) {
        this.idEditorialCategoria = idEditorialCategoria;
    }

    public Collection<Libro> getLibroCollection() {
        return libroCollection;
    }

    public void setLibroCollection(Collection<Libro> libroCollection) {
        this.libroCollection = libroCollection;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Editorial getEditorial() {
        return editorial;
    }

    public void setEditorial(Editorial editorial) {
        this.editorial = editorial;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEditorialCategoria != null ? idEditorialCategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EditorialCategoria)) {
            return false;
        }
        EditorialCategoria other = (EditorialCategoria) object;
        if ((this.idEditorialCategoria == null && other.idEditorialCategoria != null) || (this.idEditorialCategoria != null && !this.idEditorialCategoria.equals(other.idEditorialCategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ec.com.library.core.entities.EditorialCategoria[ idEditorialCategoria=" + idEditorialCategoria + " ]";
    }
    
}
