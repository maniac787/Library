package ec.com.library.core.repositories.administracion.impl;

import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Editorial;
import ec.com.library.core.repositories.HibernateRepository;
import ec.com.library.core.repositories.administracion.IEditorialRepository;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Roberto on 4/4/2018.
 */
public class EditorialRepositoryImpl extends HibernateRepository implements IEditorialRepository {

    @Override
    public Editorial buscar(Integer id) throws LibraryGenericException {
        Criteria criteria = getSession().createCriteria(Editorial.class);
        criteria.add(Restrictions.eq("idEditorial", id));
        return (Editorial) criteria.uniqueResult();
    }

    @Override
    public List<Editorial> buscarTodos() throws LibraryGenericException {
        Criteria criteria = getSession().createCriteria(Editorial.class);
        return criteria.list();
    }

    @Override
    public void insertar(Editorial entity) throws LibraryGenericException {
        getSession().persist(entity);
    }

    @Override
    public Editorial actualizar(Editorial entity) throws LibraryGenericException {
        getSession().merge(entity);
        return entity;
    }

    @Override
    public void borrar(Editorial entity) throws LibraryGenericException {
        entity.setEliminado(Boolean.FALSE);
        entity.setActivo(Boolean.FALSE);
        getSession().merge(entity);
    }
}
