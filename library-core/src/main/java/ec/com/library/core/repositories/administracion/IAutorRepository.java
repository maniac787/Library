package ec.com.library.core.repositories.administracion;

import ec.com.library.core.entities.Autor;
import ec.com.library.core.repositories.ICrud;

/**
 * Created by Roberto on 4/4/2018.
 */
public interface IAutorRepository extends ICrud<Autor> {

}
