package ec.com.library.core.repositories.administracion.impl;

import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.EditorialCategoria;
import ec.com.library.core.repositories.HibernateRepository;
import ec.com.library.core.repositories.administracion.IEditorialCategoriaRepository;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Roberto on 4/4/2018.
 */
public class EditorialCategoriaRepositoryImpl extends HibernateRepository implements IEditorialCategoriaRepository {

    @Override
    public EditorialCategoria buscar(Integer id) throws LibraryGenericException {
        Criteria criteria = getSession().createCriteria(EditorialCategoria.class);
        criteria.add(Restrictions.eq("idEditorialCategoria", id));
        return (EditorialCategoria) criteria.uniqueResult();
    }

    @Override
    public List<EditorialCategoria> buscarTodos() throws LibraryGenericException {
        Criteria criteria = getSession().createCriteria(EditorialCategoria.class);
        return criteria.list();
    }

    @Override
    public void insertar(EditorialCategoria entity) throws LibraryGenericException {
        getSession().persist(entity);
    }

    @Override
    public EditorialCategoria actualizar(EditorialCategoria entity) throws LibraryGenericException {
        getSession().merge(entity);
        return entity;
    }

    @Override
    public void borrar(EditorialCategoria entity) throws LibraryGenericException {
        entity.setEliminado(Boolean.FALSE);
        entity.setActivo(Boolean.FALSE);
        getSession().merge(entity);
    }
}
