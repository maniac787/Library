/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.library.core.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author Roberto
 */
@Entity
@Table(name = "estanteria")
@NamedQueries({
        @NamedQuery(name = "Estanteria.findAll", query = "SELECT e FROM Estanteria e")})
public class Estanteria extends Audit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_estanteria")
    private Integer idEstanteria;
    @Column(name = "numero")
    private Integer numero;
    @JoinColumn(name = "id_categoria", referencedColumnName = "id_categoria")
    @ManyToOne(fetch = FetchType.EAGER)
    private Categoria categoria;

    public Estanteria() {
    }

    public Estanteria(Integer idEstanteria) {
        this.idEstanteria = idEstanteria;
    }

    public Integer getIdEstanteria() {
        return idEstanteria;
    }

    public void setIdEstanteria(Integer idEstanteria) {
        this.idEstanteria = idEstanteria;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstanteria != null ? idEstanteria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estanteria)) {
            return false;
        }
        Estanteria other = (Estanteria) object;
        if ((this.idEstanteria == null && other.idEstanteria != null) || (this.idEstanteria != null && !this.idEstanteria.equals(other.idEstanteria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ec.com.library.core.entities.Estanteria[ idEstanteria=" + idEstanteria + " ]";
    }

}
