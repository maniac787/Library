package ec.com.library.core.repositories.administracion;

import ec.com.library.core.entities.Libro;
import ec.com.library.core.repositories.ICrud;

/**
 * Created by Roberto on 4/5/2018.
 */
public interface ILibroRepository extends ICrud<Libro> {
}
