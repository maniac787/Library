package ec.com.library.core.repositories.administracion.impl;

import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Autor;
import ec.com.library.core.entities.LibroAutor;
import ec.com.library.core.repositories.HibernateRepository;
import ec.com.library.core.repositories.administracion.ILibroAutorRepository;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Roberto on 4/4/2018.
 */
public class LibroAutorRepositoryImpl extends HibernateRepository implements ILibroAutorRepository {

    @Override
    public LibroAutor buscar(Integer id) throws LibraryGenericException {
        Criteria criteria = getSession().createCriteria(LibroAutor.class);
        criteria.add(Restrictions.eq("idLibroAutor", id));
        return (LibroAutor) criteria.uniqueResult();
    }

    @Override
    public List<LibroAutor> buscarTodos() throws LibraryGenericException {
        Criteria criteria = getSession().createCriteria(LibroAutor.class);
        return criteria.list();
    }

    @Override
    public void insertar(LibroAutor entity) throws LibraryGenericException {
        getSession().persist(entity);
    }

    @Override
    public LibroAutor actualizar(LibroAutor entity) throws LibraryGenericException {
        getSession().merge(entity);
        return entity;
    }

    @Override
    public void borrar(LibroAutor entity) throws LibraryGenericException {
        entity.setEliminado(Boolean.FALSE);
        entity.setActivo(Boolean.FALSE);
        getSession().merge(entity);
    }
}
