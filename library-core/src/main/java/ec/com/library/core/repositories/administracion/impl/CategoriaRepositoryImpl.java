package ec.com.library.core.repositories.administracion.impl;

import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Categoria;
import ec.com.library.core.repositories.HibernateRepository;
import ec.com.library.core.repositories.administracion.ICategoriaRepository;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Roberto on 4/4/2018.
 */
public class CategoriaRepositoryImpl extends HibernateRepository implements ICategoriaRepository {

    @Override
    public Categoria buscar(Integer id) throws LibraryGenericException {
        Criteria criteria = getSession().createCriteria(Categoria.class);
        criteria.add(Restrictions.eq("idCategoria", id));
        return (Categoria) criteria.uniqueResult();
    }

    @Override
    public List<Categoria> buscarTodos() throws LibraryGenericException {
        Criteria criteria = getSession().createCriteria(Categoria.class);
        return criteria.list();
    }

    @Override
    public void insertar(Categoria entity) throws LibraryGenericException {
        getSession().persist(entity);
    }

    @Override
    public Categoria actualizar(Categoria entity) throws LibraryGenericException {
        getSession().merge(entity);
        return entity;
    }

    @Override
    public void borrar(Categoria entity) throws LibraryGenericException {
        entity.setEliminado(Boolean.FALSE);
        entity.setActivo(Boolean.FALSE);
        getSession().merge(entity);
    }
}
