package ec.com.library.core.repositories.administracion.impl;

import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Autor;
import ec.com.library.core.entities.Libro;
import ec.com.library.core.repositories.HibernateRepository;
import ec.com.library.core.repositories.administracion.ILibroRepository;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Roberto on 4/4/2018.
 */
public class LibroRepositoryImpl extends HibernateRepository implements ILibroRepository {

    @Override
    public Libro buscar(Integer id) throws LibraryGenericException {
        Criteria criteria = getSession().createCriteria(Libro.class);
        criteria.add(Restrictions.eq("idLibro", id));
        return (Libro) criteria.uniqueResult();
    }

    @Override
    public List<Libro> buscarTodos() throws LibraryGenericException {
        Criteria criteria = getSession().createCriteria(Libro.class);
        return criteria.list();
    }

    @Override
    public void insertar(Libro entity) throws LibraryGenericException {
        getSession().persist(entity);
    }

    @Override
    public Libro actualizar(Libro entity) throws LibraryGenericException {
        getSession().merge(entity);
        return entity;
    }

    @Override
    public void borrar(Libro entity) throws LibraryGenericException {
        entity.setEliminado(Boolean.FALSE);
        entity.setActivo(Boolean.FALSE);
        getSession().merge(entity);
    }
}
