package ec.com.library.core.repositories.administracion;

import ec.com.library.core.entities.LibroAutor;
import ec.com.library.core.repositories.ICrud;

/**
 * Created by Roberto on 4/5/2018.
 */
public interface ILibroAutorRepository extends ICrud<LibroAutor> {
}
