package ec.com.library.core.repositories.administracion.impl;

import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Autor;
import ec.com.library.core.repositories.HibernateRepository;
import ec.com.library.core.repositories.administracion.IAutorRepository;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Roberto on 4/4/2018.
 */
public class AutorRepositoryImpl extends HibernateRepository implements IAutorRepository {

    @Override
    public Autor buscar(Integer id) throws LibraryGenericException {
        Criteria criteria = getSession().createCriteria(Autor.class);
        criteria.add(Restrictions.eq("idAutor", id));
        return (Autor) criteria.uniqueResult();
    }

    @Override
    public List<Autor> buscarTodos() throws LibraryGenericException {
        Criteria criteria = getSession().createCriteria(Autor.class);
        return criteria.list();
    }

    @Override
    public void insertar(Autor entity) throws LibraryGenericException {
        getSession().persist(entity);
    }

    @Override
    public Autor actualizar(Autor entity) throws LibraryGenericException {
        getSession().merge(entity);
        return entity;
    }

    @Override
    public void borrar(Autor entity) throws LibraryGenericException {
        entity.setEliminado(Boolean.FALSE);
        entity.setActivo(Boolean.FALSE);
        getSession().merge(entity);
    }
}
