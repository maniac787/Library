package ec.com.library.core.repositories.administracion.impl;

import ec.com.library.core.Exception.LibraryGenericException;
import ec.com.library.core.entities.Autor;
import ec.com.library.core.entities.Estanteria;
import ec.com.library.core.repositories.HibernateRepository;
import ec.com.library.core.repositories.administracion.IEstanteriaRepository;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Roberto on 4/4/2018.
 */
public class EstanteriaRepositoryImpl extends HibernateRepository implements IEstanteriaRepository {

    @Override
    public Estanteria buscar(Integer id) throws LibraryGenericException {
        Criteria criteria = getSession().createCriteria(Autor.class);
        criteria.add(Restrictions.eq("idEstanteria", id));
        return (Estanteria) criteria.uniqueResult();
    }

    @Override
    public List<Estanteria> buscarTodos() throws LibraryGenericException {
        Criteria criteria = getSession().createCriteria(Estanteria.class);
        return criteria.list();
    }

    @Override
    public void insertar(Estanteria entity) throws LibraryGenericException {
        getSession().persist(entity);
    }

    @Override
    public Estanteria actualizar(Estanteria entity) throws LibraryGenericException {
        getSession().merge(entity);
        return entity;
    }

    @Override
    public void borrar(Estanteria entity) throws LibraryGenericException {
        entity.setEliminado(Boolean.FALSE);
        entity.setActivo(Boolean.FALSE);
        getSession().merge(entity);
    }
}
