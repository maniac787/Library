package ec.com.library.core.Exception;

/**
 * Created by Roberto on 4/5/2018.
 */
public class LibraryGenericException extends Exception {
    public LibraryGenericException() {
        super();
    }

    public LibraryGenericException(String message) {
        super(message);
    }

    public LibraryGenericException(String message, Throwable cause) {
        super(message, cause);
    }

    public LibraryGenericException(Throwable cause) {
        super(cause);
    }

    protected LibraryGenericException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
