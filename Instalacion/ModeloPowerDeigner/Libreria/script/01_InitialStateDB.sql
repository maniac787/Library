/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     4/4/2018 11:34:02 PM                         */
/*==============================================================*/


drop table AUTOR;

drop table CATEGORIA;

drop table EDITORIAL;

drop table EDITORIAL_CATEGORIA;

drop table ESTANTERIA;

drop table LIBRO;

drop table LIBRO_AUTOR;

/*==============================================================*/
/* Table: AUTOR                                                 */
/*==============================================================*/
create table AUTOR (
   ID_AUTOR             SERIAL               not null,
   NOMBRE               VARCHAR(256)         null,
   APELLIDO             VARCHAR(256)         null,
   IDENTIFICACION       VARCHAR(256)         null,
   ACTIVO               BOOL                 null,
   ELIMINADO            BOOL                 null,
   FECHA_CREACION       TIMESTAMP            null,
   FECHA_ACTUALIZACION  TIMESTAMP            null,
   constraint PK_AUTOR primary key (ID_AUTOR)
);

/*==============================================================*/
/* Table: CATEGORIA                                             */
/*==============================================================*/
create table CATEGORIA (
   ID_CATEGORIA         SERIAL               not null,
   CODIGO               VARCHAR(256)         null,
   NOMBRE               VARCHAR(256)         null,
   ACTIVO               BOOL                 null,
   ELIMINADO            BOOL                 null,
   FECHA_CREACION       TIMESTAMP            null,
   FECHA_ACTUALIZACION  TIMESTAMP            null,
   constraint PK_CATEGORIA primary key (ID_CATEGORIA)
);

/*==============================================================*/
/* Table: EDITORIAL                                             */
/*==============================================================*/
create table EDITORIAL (
   ID_EDITORIAL         SERIAL               not null,
   NOMBRE               VARCHAR(256)         null,
   MAIL                 VARCHAR(256)         null,
   DIRECCION            VARCHAR(256)         null,
   ACTIVO               BOOL                 null,
   ELIMINADO            BOOL                 null,
   FECHA_CREACION       TIMESTAMP            null,
   FECHA_ACTUALIZACION  TIMESTAMP            null,
   constraint PK_EDITORIAL primary key (ID_EDITORIAL)
);

/*==============================================================*/
/* Table: EDITORIAL_CATEGORIA                                   */
/*==============================================================*/
create table EDITORIAL_CATEGORIA (
   ID_EDITORIAL_CATEGORIA SERIAL               not null,
   ID_EDITORIAL         INT4                 null,
   ID_CATEGORIA         INT4                 null,
   ACTIVO               BOOL                 null,
   ELIMINADO            BOOL                 null,
   FECHA_CREACION       TIMESTAMP            null,
   FECHA_ACTUALIZACION  TIMESTAMP            null,
   constraint PK_EDITORIAL_CATEGORIA primary key (ID_EDITORIAL_CATEGORIA)
);

/*==============================================================*/
/* Table: ESTANTERIA                                            */
/*==============================================================*/
create table ESTANTERIA (
   ID_ESTANTERIA        SERIAL               not null,
   ID_CATEGORIA         INT4                 null,
   NUMERO               INT4                 null,
   ACTIVO               BOOL                 null,
   ELIMINADO            BOOL                 null,
   FECHA_CREACION       TIMESTAMP            null,
   FECHA_ACTUALIZACION  TIMESTAMP            null,
   constraint PK_ESTANTERIA primary key (ID_ESTANTERIA)
);

/*==============================================================*/
/* Table: LIBRO                                                 */
/*==============================================================*/
create table LIBRO (
   ID_LIBRO             SERIAL               not null,
   ID_EDITORIAL_CATEGORIA INT4                 null,
   ISBN                 VARCHAR(256)         not null,
   TITULO               VARCHAR(256)         not null,
   PAGINAS              INT4                 null,
   ACTIVO               BOOL                 null,
   ELIMINADO            BOOL                 null,
   FECHA_CREACION       TIMESTAMP            not null,
   FECHA_ACTUALIZACION  TIMESTAMP            null,
   constraint PK_LIBRO primary key (ID_LIBRO)
);

/*==============================================================*/
/* Table: LIBRO_AUTOR                                           */
/*==============================================================*/
create table LIBRO_AUTOR (
   ID_LIBRO_AUTOR       SERIAL               not null,
   ID_AUTOR             INT4                 null,
   ID_LIBRO             INT4                 null,
   ACTIVO               BOOL                 null,
   ELIMINADO            BOOL                 null,
   FECHA_CREACION       TIMESTAMP            null,
   FECHA_ACTUALIZACION  TIMESTAMP            null,
   constraint PK_LIBRO_AUTOR primary key (ID_LIBRO_AUTOR)
);

alter table EDITORIAL_CATEGORIA
   add constraint FK_EDITORIA_REFERENCE_EDITORIA foreign key (ID_EDITORIAL)
      references EDITORIAL (ID_EDITORIAL)
      on delete restrict on update restrict;

alter table EDITORIAL_CATEGORIA
   add constraint FK_EDITORIA_REFERENCE_CATEGORI foreign key (ID_CATEGORIA)
      references CATEGORIA (ID_CATEGORIA)
      on delete restrict on update restrict;

alter table ESTANTERIA
   add constraint FK_ESTANTER_REFERENCE_CATEGORI foreign key (ID_CATEGORIA)
      references CATEGORIA (ID_CATEGORIA)
      on delete restrict on update restrict;

alter table LIBRO
   add constraint FK_LIBRO_REFERENCE_EDITORIA foreign key (ID_EDITORIAL_CATEGORIA)
      references EDITORIAL_CATEGORIA (ID_EDITORIAL_CATEGORIA)
      on delete restrict on update restrict;

alter table LIBRO_AUTOR
   add constraint FK_LIBRO_AU_REFERENCE_AUTOR foreign key (ID_AUTOR)
      references AUTOR (ID_AUTOR)
      on delete restrict on update restrict;

alter table LIBRO_AUTOR
   add constraint FK_LIBRO_AU_REFERENCE_LIBRO foreign key (ID_LIBRO)
      references LIBRO (ID_LIBRO)
      on delete restrict on update restrict;

